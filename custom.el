(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("855eb24c0ea67e3b64d5d07730b96908bac6f4cd1e5a5986493cbac45e9d9636" default)))
 '(package-selected-packages
   (quote
    (hydra page-break-lines ergoemacs-mode evil-org evil-indent-textobject evil-surround evil-leader evil ivy-posframe centaur-tabs dashboard neotree olivetti org-superstar org-superstar-mode org-fragtog magit which-key ledger-mode counsel swiper ivy company org-plus-contrib switch-window dmenu exwm smart-mode-line-atom-one-dark-theme atom-one-dark-theme pretty-mode async undo-tree use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "#282C34" :foreground "#ABB2BF" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 130 :width normal :foundry "PfEd" :family "Fantasque Sans Mono"))))
 '(org-document-title ((t (:weight bold :height 1.5))))
 '(org-ellipsis ((t (:foreground "LightGoldenrod")))))
